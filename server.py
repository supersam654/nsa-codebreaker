from redis import Redis
from rq import Queue
import datetime

import brute

DAYS = 1000

q = Queue(connection=Redis())

base = datetime.datetime.today()
date_list = [base - datetime.timedelta(days=x) for x in range(0, DAYS)]
for i in range(len(date_list) - 1):
    start_time = int(date_list[i].strftime('%s'))
    end_time = int(date_list[i+1].strftime('%s'))
    q.enqueue_call(
        func=brute.brute,
        args=(start_time, end_time),
        result_ttl=86400 * 2
        timeout=7200)
