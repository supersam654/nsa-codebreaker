from os import system, getpid, chdir
import time
from redis import Redis
from rq import Worker, Queue, Connection

SERIAL = 920284542
CORRECT_OTP_STRING = 'otpauth://totp/920284542?secret=AOZ4HVCIMV7FFWZ52GKH47ZRDUQDODDKODAXZC3NEYJR5XOF6CEQ'
START = 1473510000

PID = getpid()

def setup():
    system('mkdir -p /tmp/%d_brute' % PID)
    system('cp keygen /tmp/%d_brute' % PID)
    system('cp gen /tmp/%d_brute' % PID)
    chdir('/tmp/%d_brute' % PID)

def set_time(new_time):
  system('./gen %d out' % new_time)

def get_otpstring(serial, new_time):
  set_time(new_time)
  system('./keygen -m out -k %d -o new_out' % (PID, serial))
  with open('%d.key' % serial) as f:
    return f.read().strip()

def brute(end_seconds, start_seconds):
    setup()
    print("Bruting: %d to %d (%d)" % (start_seconds, end_seconds, end_seconds - start_seconds))
    for i in xrange(start_seconds, end_seconds):
        otp_string = get_otpstring(SERIAL, i)
        if i % 10000 == 0:
            print('%d: %d: %s' % (i, len(otp_string), otp_string))
        if otp_string == CORRECT_OTP_STRING:
            print('HEREBEDRAGONS: %d: %s' % (i, otp_string))
            return otp_string
