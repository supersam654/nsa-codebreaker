#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>

int main(int argc, char *argv[]) {
        if (argc < 3) {
                return 1;
        }
        int seed = atoi(argv[1]);
        //printf("%d", seed);
        unsigned int buffer[8];
        srand(seed);
        buffer[0] = rand();
        buffer[1] = rand();
        buffer[2] = rand();
        buffer[3] = rand();
        buffer[4] = rand();
        buffer[5] = rand();
        buffer[6] = rand();
        buffer[7] = rand();
        FILE *writer;
        writer = fopen(argv[2], "wb");
        fwrite(buffer, sizeof(buffer), 1, writer);
        return 0;
}
